This is an experimental and unofficial manifestation of the codified laws of Schenectady County.
It is intended to test whether the AsciiDoctor toolchain is a viable way to represent and publish a body of local laws.

The root folder contains the three works that comprise Schenectady County's codification:
* Schenectady County Charter
* Schenectady County Administrative Code
* Schenectady County Codified Laws

An additional file assembles these three parts into a single document.

The contents of each work are stored under its respective folder.
The code source is broken into separate files for each section of the document to keep them modular.
Annotations from the official codification are generally embedded as footnotes.
The document contains some embedded tables and images.

Also included is a script `compile` which can build the root-level files and all their included parts into PDF, HTML, DocBook XML, and EPUB.