[[code-section-002-01, 2.1]]
=== Section 2.01. Authorization for conference expenses.

A. The chairperson of the legislature shall have the power to designate and authorize any member, officer or employee of the legislative branch to attend an official or unofficial convention, conference or school for the betterment of county government.
B. Within the appropriation therefore and when so authorized all necessary and actual expenses including but not limited to a registration fee and mileage as fixed by such board shall be paid from county funds.
