[[code-section-003-02, 3.2]]
=== Section 3.02. Conflict of interest.

The county manager shall not be an officer, director or stockholder of any depositary or depositaries designated by him or her pursuant to the powers and duties granted by the county charter.
