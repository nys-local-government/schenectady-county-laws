== ARTICLE IV. Department of Law

include::article-004/004.00.asc[]

include::article-004/004.01.asc[]

include::article-004/004.02.asc[]

include::article-004/004.03.asc[]

include::article-004/004.04.asc[]

include::article-004/004.05.asc[]

include::article-004/004.06.asc[]
