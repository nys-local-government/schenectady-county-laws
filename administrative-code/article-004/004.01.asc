[[code-section-004-01, 4.1]]
=== Section 4.01. Acting county attorney, how designated.

If more than one deputy county attorney or assistant county attorney shall be appointed, the county attorney shall designate in writing and file in the office of the county clerk and the clerk of the legislature the order in which such deputies and/or assistants shall exercise the powers and duties of the office in the event of a vacancy or the absence or the inability of such county attorney to perform the duties of the office.
