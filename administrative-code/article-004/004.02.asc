[[code-section-004-02, 4.2]]
=== Section 4.02. Inconsistent interests among county officials.

Whenever the interests of the legislature or the county are inconsistent with the interests of any officer paid his or her compensation from county funds, the county attorney shall represent the interests of the legislature and the county. In such case the officer may at his or her own expense employ an attorney at law.
