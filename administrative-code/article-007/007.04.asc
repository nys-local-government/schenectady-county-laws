[[code-section-007-04, 7.4]]
=== Section 7.04. Contingent fund.

The legislature by resolution may, at any time, appropriate all or any part of the monies in the general contingent fund for general county purposes.
