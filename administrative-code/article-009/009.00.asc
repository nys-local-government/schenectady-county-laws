[[code-section-009-00, 9.0]]
=== Section 9.00. Medical Examiner, appointment, revocation.

A. Upon appointment of a medical examiner as provided in section 14.07 of the county charter, a certificate of such appointment shall be filed and recorded in the office of the county clerk.
B. The medical examiner, before entering upon the duties of his or her office, shall take and file the prescribed oath of office and furnish and file the required undertaking.
C. The appointment of the medical examiner may be revoked at any time by the county manager by the filing of a certificate of such revocation in the office of the county clerk.
