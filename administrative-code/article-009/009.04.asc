[[code-section-009-04, 9.4]]
=== Section 9.04. Employment of stenographer.

A. When the services of a stenographer shall not have been provided by the legislature, or if a stenographer so provided is not available, the medical examiner shall have the power to employ a stenographer for the purpose of taking statements and reducing to writing the testimony of witnesses or of transcribing or reproducing any report or document required by his or her investigation.
B. If the legislature has not fixed any rate of compensation, such stenographer shall be paid for taking and transcribing minutes at the rate charged by official court stenographers in the County of Schenectady.
