[[code-section-014-00, 14.0]]
=== Section 14.00. Divisions within the department of engineering and public works.footnote:[Added by L.L. 2-2015; former section 14.00 redesignated 14.01 by L.L. 2-2015.]

The department of engineering and public works shall be composed of a division of public works, a division of engineering, a division of parks and a division of facilities.
