[[code-section-040-04, 40.4]]
=== Section 40.04. Divisions within the Schenectady County administrative code.footnote:[Added by L.L. 2-2015.]

The administrative code is divided, in descending order of application, by article, section, subdivision, paragraph and subparagraph.
