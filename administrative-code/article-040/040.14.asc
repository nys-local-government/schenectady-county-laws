[[code-section-040-14, 40.14]]
=== Section 40.14. Judicial notice.footnote:[Formerly section 20.05; redesignated by L.L. 2-2015.]

All courts shall take judicial notice of all laws contained in this code and of all local laws, acts, resolutions, rules, and regulations adopted pursuant to the county charter or this code.
