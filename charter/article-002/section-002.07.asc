[[charter-section-002-07, 2.7]]
=== Section 2.07. Clerk of the county legislature.footnote:[Amended by L.L. 9-1971, § 3.]

On January 1st of the even numbered year next following the election of any members of the county legislature, or as soon thereafter as practicable, the county legislature shall appoint a clerk who shall serve at the pleasure of the county legislature and until his or her successor is appointed and has qualified, and from time to time the clerk shall appoint such additional personnel as are required for the efficient operation of the office of the clerk, within budgetary limitations.
