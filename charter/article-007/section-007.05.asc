[[charter-section-007-05, 7.5]]
=== Section 7.05. County legislature action.

A. After the public hearing, the county legislature may adopt the budget with or without amendment. In amending, it may add, delete, increase or decrease items except for appropriations for debt service and any other appropriations required by law.
B. After the public hearing, the county legislature may also adopt the capital program with or without amendment. In amending, it may delete and decrease any item.
C. Any additions and increases must wait for the recommendations of the county manager which the legislature must request and consider, but need not follow.
D. If a requested recommendation is not presented to the legislature within 5 days after the request therefore, the legislature may make such additions and increases without such recommendation.
E. The legislature shall adopt the budget and capital program on or before the first day of November. If it fails to do so the budget and capital program shall be deemed adopted by the legislature as submitted by the manager.
F. Three copies of the budget and capital program as adopted shall be certified by the county manager and the clerk of the county legislature.
  1. One of these copies shall be filed in the office of the county manager and one each in the offices of the commissioner of finance and the clerk of the county legislature.
  2. The budget and capital program so certified shall be printed or otherwise reproduced and a copy shall be made available upon request from any county resident.
