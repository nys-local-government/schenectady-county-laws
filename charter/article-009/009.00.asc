[[charter-section-009-00, 9.0]]
=== Section 9.00. Real property tax service agency; director.footnote:[Added by L.L. 2-1985, § 11; formerly section 21.10; redesignated by L.L. 2-2015.]

A. There shall be a real property tax service agency headed by a director who shall be appointed by the county manager, subject to confirmation by the county legislature.
B. The operation of the real property tax service agency shall be provided and administered in accordance with the provisions of the New York State Real Property Tax Law, administrative code and by applicable law.
