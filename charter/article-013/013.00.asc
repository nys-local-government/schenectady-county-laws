[[charter-section-013-00, 13.0]]
=== Section 13.00. Department of social services; commissioner.footnote:[Amended by L.L. 2-1985, § 5.]

A. There shall be a department of social services headed by a commissioner appointed by the county manager, subject to confirmation by the county legislature.
B. The commissioner of social services shall have received a bachelor's degree from an accredited college or university, and shall possess the qualifications required by state law.
C. The commissioner of social services shall be appointed for a 5 year term.
