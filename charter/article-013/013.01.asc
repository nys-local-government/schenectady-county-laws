[[charter-section-013-01, 13.1]]
=== Section 13.01. Powers and duties.footnote:[Amended by L.L. 2-1985, § 5; L.L. 2-2006, § 1.]

A. The commissioner of social services shall have and exercise all powers and duties now or hereafter conferred or imposed upon him or her by applicable law, including management and supervision of any welfare institutions of the county.
B. The commissioner of social services shall perform such other and related duties as shall be delegated to him or her by the county manager.
