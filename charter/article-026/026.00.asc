[[charter-section-026-00, 26.0]]
=== Section 26.00. Office of security services; chief security officer.footnote:[Added by L.L. 6-1996, § 1; formerly section 21.13(2); redesignated and amended by L.L. 2-2015.]

A. There shall be an office of security services, subject to appropriation by the county legislature, headed by a chief security officer who shall be appointed by the county manager on the basis of his or her experience and qualifications for the office.
B. The chief security officer shall, under the supervision of the county manager, have charge of security of all county buildings.
C. The chief security officer shall perform such other duties as may be directed by the county manager.
D. The chief security officer shall be authorized to employ retired former members of the police or sheriff’s departments, or the division of state police or retired former correction, parole or probation officers for the purpose of providing special patrol officers in properties owned or leased by the county in order to protect the property or persons on such premises.
