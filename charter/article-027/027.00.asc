[[charter-section-027-00, 27.0]]
=== Section 27.00. Residential health care facility.footnote:[Added by L.L. 2-2015.]

A. There shall be a residential health care facility headed by an administrator who shall be appointed by the county manager, subject to confirmation by the county legislature. 
B. The administrator shall have the qualifications of a nursing home administrator. 
C. The administrator shall have and exercise all powers and duties now or hereafter conferred or imposed upon him or her by applicable law which shall be necessary to enable him or her to manage and supervise the county residential health care facility.
