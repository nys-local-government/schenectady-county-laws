[[charter-section-040-01, 40.1]]
=== Section 40.01. Effective offices abolished; incumbents continued.

A. The elective offices of county treasurer, commissioner of public welfare, and coroner are abolished, effective January 1, 1966.
B. The persons holding such offices on January 1, 1966, shall be continued in the appointive positions relating to their functions for the remainder of their respective terms of office, at the conclusion of which the provisions of this charter relating to the appointment of a county treasurer, commissioner of social welfare and medical examiner shall take effect.
