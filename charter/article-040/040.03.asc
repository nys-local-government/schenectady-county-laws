[[charter-section-040-03, 40.3]]
=== Section 40.03. Continuity of authority.

All existing state, county, local and other laws or enactments having the force of law shall continue in force until lawfully amended, modified, superseded or repealed, either by this charter or an enactment adopted subsequent to its effective date. Any proceedings or other business undertaken or commenced prior to the effective date of this charter may be conducted and completed by the county officer or administrative unit responsible therefore under this charter or the administrative code.
