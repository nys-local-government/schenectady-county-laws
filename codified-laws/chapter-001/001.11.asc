[[codified-section-001-11, 1.11]]
=== Section 1.11. Headings, captions, footnotes and editorial notes.

Headings, captions, footnotes and editorial notes used in the Schenectady County codified local laws other than the article, chapter, and section numbers are employed for reference purposes only and shall not be deemed to be a part of the text of any section.
