[[codified-section-001-16, 1.16]]
=== Section 1.16. Manifest errors and omissions.

A. If a manifest error is discovered by the clerk of the legislature consisting of the misspelling of any words; the omission of any word or words necessary to express the intention of the provisions affected; the use of a word or words to which no meaning can be attached; or the use of a word or words when another word or words was clearly intended to express such intent, such spelling shall be corrected and such word or words supplied, omitted, or substituted as will conform with the manifest intention, and the provisions shall have the same effect as though the correct words were contained in the text as originally published.
B. No alteration shall be made or permitted if any questions exist regarding the nature or extent of such error.
C. Any alterations shall be filed by the clerk of the legislature with the department of state.
