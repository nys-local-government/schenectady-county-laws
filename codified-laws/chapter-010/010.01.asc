[[codified-section-010-01, 10.1]]
=== Section 10.01. Legislative intent.

This legislature hereby finds and determines that:

A. "AWOL," an acronym for Alcohol WithOut Liquid, is a device that mixes alcoholic beverages with pure oxygen. A cloudy alcoholic vapor is created by this device when an alcoholic beverage is poured into a "diffuser capsule" that is connected to an oxygen pipe. The resultant vapor can then be inhaled.
B. An AWOL device enables people to inhale vaporized alcoholic drinks such as vodka or gin through the mouth or nasal passages by using a tube.
C. By bypassing the stomach and the liver, the alcoholic vapor is absorbed through blood vessels in the nasal passages or lungs, creating a quicker and more intense effect on the brain.
D. Experts have claimed that the practice of inhaling alcoholic vapor can be harmful to one's health, safety and physical well-being.
E. These AWOL devices are being marketed as a simpler and quicker way to become intoxicated, and as a "dieter's dream" because one can consume alcoholic beverages in a manner that does not involve calories.
F. Providing devices that can promote alternative methods of inducing intoxication to young adults can create additional circumstances whereby the health, safety and physical well-being of the public may be jeopardized.
G. The purpose of this local law is to ban the sale, purchase, possession and use of AWOL devices in Schenectady County.
