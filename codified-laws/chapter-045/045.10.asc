[[codified-section-045-10, 45.10]]
=== Section 45.10. Pre-emption.

This local law shall be pre-empted by a New York law relating to item pricing of consumer commodities.
