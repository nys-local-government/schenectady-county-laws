[[codified-section-050-05, 50.5]]
=== Section 50.05. Pricing accuracy inspection and penalties.

A. The following penalties are established for pricing accuracy inspection violations, based on a sample of not fewer than 100, nor more than 300 stock keeping units, or 25 stock keeping units in retail stores, which offer for sale fewer than 100 stock keeping units.
  The pricing accuracy inspection shall represent a cross section of all stock keeping units for sale to ascertain that the retail price is the same as the computer assisted checkout price.
  A violation exists when the programmed computer price exceeds the retail price:
+
  1. 98% or better pricing accuracy, pricing corrected;
  2. 97% pricing accuracy, a penalty of five hundred dollars ($500);
  3. 96% pricing accuracy, a penalty of seven hundred fifty dollars ($750);
  4. 95% pricing accuracy, a penalty of one thousand dollars ($1,000); and
  5. Below 95% pricing accuracy, a penalty of one thousand five hundred dollars ($1,500)

B. If a retail store fails to achieve a pricing accuracy level of 95% on 2 consecutive pricing accuracy inspections, a penalty in the amount of two thousand dollars ($2,000) shall be assessed.
