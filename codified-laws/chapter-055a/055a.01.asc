[[codified-section-055a.01, Section 055a.01]]
=== Section 55-A.01. Short title.

This Plan shall be known as the Schenectady County Bar Association Plan for Representation of Indigent Litigants (the “Plan”).
