[[codified-section-080-09, 80.9]]
=== Section 80.09. Construction.

This local law shall be deemed an exercise of the powers of the county to preserve and improve the quality of the natural and man-made environment on behalf of the present and future citizens thereof. This local law is not intended and shall not be deemed to impair the powers of the city or any of the towns or villages within the County of Schenectady.
