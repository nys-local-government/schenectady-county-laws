[[codified-section-085-01, 85.1]]
=== Section 85.01. Regulation of freshwater wetlands.

A. Pursuant to section 24-0901 of the New York State Environmental Conservation Law, the County of Schenectady is authorized to enter into a cooperative agreement with the commissioner of the New York State department of environmental conservation for the purpose of preserving and maintaining those freshwater wetlands which are within the boundaries of the county.

B. The cooperative agreement shall provide that the freshwater wetlands be preserved and maintained in their natural state and may provide for access thereto to be retained by such owner for purposes compatible with the purposes of article 24 of the New York State Environmental Conservation Law.

C. The cooperative agreement may provide for use of personnel and facilities of the New York State department of environmental conservation, or the payment out of funds appropriated therefore, for the purpose of preserving, maintaining, or enhancing such wetlands, and for the furnishing of such personnel, facilities or funds as may be agreed upon by the parties to the cooperative agreement.
