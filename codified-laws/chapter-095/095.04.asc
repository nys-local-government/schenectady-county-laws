[[codified-section-095-04, 95.4]]
=== Section 95.04. Board of Ethics.

A. Establishment.
+
Pursuant to article 18 of the General Municipal Law, the Schenectady County board of ethics is hereby established.
It shall be responsible for ensuring full compliance with the Schenectady County ethics and disclosure law.

B. Membership.
+
The board of ethics shall consist of 5 members.

1. Members shall be appointed by the county manager subject to confirmation by the county legislature.

2. No more than 2 members shall be of the same enrolled party affiliation.

3. No more than one member shall presently be a county officer or employee, and none shall presently hold elected office.
The first meeting of the board of ethics in each calendar year shall be the organizational meeting; at such meeting the members shall elect a Chair who shall be a member thereof.

4. Members of the board of ethics shall serve without compensation, but shall be entitled to reimbursement of reasonable expenses in accordance with rules established by the county legislature.

5. The members of the board of ethics shall serve staggered 3 year terms.

6. In the event a vacancy occurs prior to the expiration of the 3 year term, such vacancy shall be filled for the balance of such term in the same manner as members are appointed to full terms.

7. All members shall reside within the County of Schenectady.

C. Removal.
+
In addition to penalties defined herein specifically for violation of the Schenectady county ethics and disclosure law, and other pertinent sections of local, state and federal law, members of the board of ethics may be removed for cause by the county manager with the concurrence of two-thirds of the county legislature.
Prior to removal, the board of ethics member shall be given written notice of the grounds for removal and an opportunity to reply.


D. Powers and Duties.

1. The board of ethics shall possess all powers and duties authorized by section 808 of the New York State General Municipal Law.

2. The board of ethics shall, on or before March 1 of each year, distribute the annual statement of financial disclosure to affected individuals with notice of the date upon which the completed form is required to be filed with the board of ethics.

3. The board of ethics shall receive and decide appeals as provided by section 95.06 herein from persons required to provide financial disclosure.

4. The board of ethics shall be the repository for completed annual statements of financial disclosure, pursuant to section 808(5) of the New York State General Municipal Law and such written instruments, affidavits, and disclosures as required thereunder.

5. The board of ethics shall possess, exercise and enjoy all the rights, powers and privileges necessary and proper to the enforcement of the Schenectady County ethics and disclosure law and completion and filing by reporting officers, employees and appointed officials of the county of annual statements of financial disclosure required thereunder.

6. The board of ethics shall promulgate rules and regulations in furtherance of its powers and duties enumerated herein.
Said rules and regulations shall include rules governing the conduct of adjudicatory proceedings and appeals relating to the assessment of the civil penalties as authorized in the Schenectady County ethics and disclosure law.

7. The county legislature may empower the board of ethics to subpoena any individual, whether or not a county officer, employee or appointed official, and any document or thing which the board of ethics deems necessary to the resolution of any pending adjudicatory proceeding or matter.

8. The board of ethics shall render advisory opinions in writing regarding specific matters pertaining to filings and reporting categories, to officers, employees and appointed officials of the county with respect to the Schenectady County ethics and disclosure law and article 18 of the New York State General Municipal Law.
Such opinions shall be rendered only upon written request by the officer, employee or appointed official concerning only the subject of the inquiry as it pertains to the requesting individual's own filing requirements.

9. Such opinions shall not be made public or disclosed unless required by the Freedom of Information Law (article 6 of the New York State Public Officers Law) or required for use in a disciplinary proceeding or proceeding under the Schenectady County ethics and disclosure law involving the officer, employee or appointed official who requested the advisory opinion.
Whenever a request for access to an advisory opinion herein is received, the officer, employee or appointed official who requested the opinion shall be notified of the request within 48 hours of the receipt of the request.
Under no circumstances shall the reporting categories shown on the annual statement of financial disclosure filed by an individual be disclosed to the public.

10. The county manager shall designate an attorney admitted to practice in the State of New York or may designate the office of the county attorney to serve as counsel to the board of ethics.

11. The board of ethics shall be empowered to request support staff assistance from the county legislature or the county manager in furtherance of its duties and responsibilities.
