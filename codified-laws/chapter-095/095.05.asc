[[codified-section-095-05, 95.5]]
=== Section 95.05. Disclosure of Interest.

A. Any officer or their relative who has, will have or intends to acquire a direct or indirect interest in any matter being considered by the legislature of the County of Schenectady or by any other official, board, agency, officer or employee of the County of Schenectady, and who participates in the discussion before or who gives an opinion or gives advice to any board, agency or individual considering the same, shall publicly disclose on the official record the nature and the extent of such interest.

B. Any officer or employee of the County of Schenectady, or their relative, who has knowledge of any matter being considered by any board, agency, officer or employee of the County of Schenectady in which matter he or she has or will have or intends to acquire any direct or indirect interest, shall be required immediately to disclose, in writing, his or her interest to such board, agency, officer or employee, and the nature and the extent thereof, to the degree that such disclosure gives substantial notice of any potential conflict of interest.

1. On or before January 31 of each year for which the employment is in effect, the chief elected official shall promulgate a list of all positions required to provide financial disclosure.
Said list shall be determined by reference to article 18 of the New York State General Municipal Law.

2. Financial disclosure shall be accomplished by completion of an annual statement of financial disclosure.

3. A person who is subject to the filing requirements of this local law from more than one county, or of an equivalent provision under New York State Law, may satisfy the requirements by filing only one annual statement of financial disclosure, and filing with the County Ethics Board a notice that such filing has been made, inclusive of the date and place of the filing.

4. Any person who is subject to the reporting requirements of this local law and who has or shall timely file with the Internal Revenue Service an application for automatic extension of time in which to file his or her individual income tax return for the immediately preceding calendar or fiscal year which would extend filing with the IRS beyond May 15 of that year shall be required to submit notice of said application on or before March 31 of the year in which the employment is in effect.
Such person shall file a completed disclosure form, absent only the portions stated within the application for automatic extension, on or before May 15 of the year for which the employment is in effect, and a supplementary statement for any item as so noted on the annual statement of financial disclosure, without liability under section 95.07 of this local law, if said supplementary statement is filed within 15 days of the expiration of the automatic extension.

5. Any person who is required to file an annual statement of financial disclosure may request, prior to May 1, of the year for which the employment is in effect, an extension of filing for an additional specific period of time.
Such request shall be made in writing to the appropriate body, with approval based upon substantiation of justifiable cause or undue hardship.
The appropriate body may grant or deny the request, by vote of the membership, and extensions shall be for the specific period of additional time requested.

6. Any person required to file an annual statement of financial disclosure who becomes so required, or experiences a change in reporting levels, or becomes a candidate for county elected office, after March 15 of the year for which the employment is in effect, shall file the appropriate annual statement within 30 days.
