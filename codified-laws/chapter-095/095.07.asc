[[codified-section-095-07, 95.7]]
=== Section 95.07. Penalties.

A. A reporting individual who knowingly and willfully fails to file an annual statement of financial disclosure or who knowingly or willfully with intent to deceive makes a false statement or gives information which such individual knows to be false on such statement of financial disclosure filed pursuant to this section shall be assessed a civil penalty in an amount not to exceed ten thousand dollars ($10,000).

B. Assessment of a civil penalty hereunder shall be made by the appropriate body.
For a violation of this section, the appropriate body may, in lieu of a civil penalty, refer a violation to the appropriate prosecutor and upon such conviction, but only after such referral, such violation shall be punishable as a Class A misdemeanor.

C. A civil penalty for false filing may not be imposed hereunder in the event a category of "value" or "amount" reported hereunder is incorrect unless such reported information is falsely understated.

D. Upon an appropriate determination and vote, the appropriate body may file a written recommendation with the chief elected official or other disciplinary body, establishing grounds for removal for cause, in accordance with other provisions of state and county law and contractual obligations of the county pertaining to officers, officials and employees and rules governing conduct.

E. Notwithstanding any other provision of law to the contrary, no other penalty, civil or criminal, may be imposed for a failure to file, or for a false filing, of such statement, except that the county manager may impose disciplinary action as otherwise provided by law.

F. The appropriate body shall adopt rules governing the conduct of adjudicatory proceedings and appeals relating to the assessment of the civil penalties herein authorized.
Such rules shall provide for due process procedural mechanisms substantially similar to those set forth in article 3 of the New York State Administrative Procedure Act but such mechanisms need not be identical in terms or scope.

G. Assessment of a civil penalty shall be final unless modified, suspended or vacated within thirty days of imposition and upon becoming final shall be subject to review at the instance of the affected reporting individual in a proceeding commenced against the appropriate body pursuant to article 78 of the New York State Civil Practice Law and Rules.
