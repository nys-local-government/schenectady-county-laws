[[codified-section-125-02, 125.2]]
=== Section 125.02. Food service program.footnote:[Adopted 6-13-2000 by Local Law 5-2000.)]

A. Adoption of State sanitary code.
+
The Schenectady County legislature officially adopts part 14 of the New York State sanitary code, as may be amended from time to time, as being applicable within Schenectady County.

B. Permits.

1. It shall be unlawful for any person to operate a food service establishment, catering establishment or temporary food service establishment within Schenectady County unless such person possesses a valid permit issued by the county public health services commissioner or public health director pursuant to this code, the New York State sanitary code and the New York State Public Health Law.

2. Prior to obtaining a permit or no later than 6 months after, at least 1 person who is an owner, operator or responsible employee of the food service establishment, must attend food service training acceptable to the permit issuing official.

3. Failure to attend the required training will result in the revocation of the food service permit.

C. Sanitary Facilities.

1. Disinfection of water supply.
+
All food service establishments opening after January 1, 1995 is required to disinfect their private water supply.
Existing food service establishments with private water supplies will be required to disinfect their water supply after 1 unsatisfactory sample with a total coliform and/or E. coli positive result taken after the adoption of this code.
Plans for disinfection of the water supply must be submitted to the department for review before it is put into use.

2. Plans for a sewage disposal system.
+
Plans for a sewage disposal system at a food service establishment must be prepared by a licensed New York State professional engineer or exempted land surveyor (section 7208 (n) of the New York State Education Law). Said plans must be submitted to the department for review before they are put into use.

3. Handwashing facilities.
+
Adequate handwashing facilities must be provided in all distinct and/or separate food preparation areas of a food service establishment.

4. Indirect drains.
+
Drains from food preparations sinks must be indirect drains in all food service establishments where food must be washed or rinsed.

5. Equipment used for food storage or preparation.
+
All equipment used for food storage or preparation in food service establishments will be evaluated as to its affect on public health and be installed in conformance to appropriate standards and good public health practices.
If installed in a food service establishment, equipment which may adversely affect public health can be required to be removed or its use discontinued.

D. Enforcement.

1. The permit-issuing official may waive, in writing, any of the requirements of subpart 14-1 of the New York State sanitary code, and the waiver included as a condition of the permit to operate, when it reasonably appears that the public health will not be endangered by such waiver.
All such waivers are to be only for the same period as the term of the permit.

2. The commissioner or public health director of Schenectady County department of public health services shall have the power to suspend or revoke the permit of any permittee for any violation of this sanitary code or part 14 of the New York State sanitary code.
