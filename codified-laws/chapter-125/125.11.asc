[[codified-section-125-11, 125.11]]
=== Section 125.11. Nuisances.

A. Nuisances: commissioner's duty to investigate.
+
The commissioner or public health director or his or her duly authorized designee shall receive and examine into all reasonable complaints made by any inhabitant of the health district concerning nuisances, or causes of danger or injury to life and health in the health district and may request such complaints to be made in writing.

B. Nuisances: investigation and reports.

1. The commissioner or public health director or his or her designee may enter upon or within any place or premises where nuisances or conditions dangerous to life and health, or which are the cause of nuisances existing elsewhere, are known or believed to exist to inspect or examine same.

2. The owners, agents and occupants of any premises shall permit sanitary examinations and inspections to be made pursuant to the provisions of this law and title 1, article 13 of the New York State Public Health Law.

3. The commissioner or public health director shall furnish the owners, agents and occupants of the premises on which such conditions exists with a written statement of the results and conclusions of an examination or inspection conducted pursuant to this article.

C. Nuisances: abatement and suppression.

1. The commissioner or public health director shall order the suppression and removal of all nuisances and conditions detrimental to life and health found to exist within the health district.

2. The commissioner or public health director or his or her designee(s) may, if the owner, agent or occupant of any premises whereon and nuisances or conditions deemed to be detrimental to public health exists or causes the existence of such nuisance or the premises to remove or suppress such nuisance, condition, or matter to which said order relates.

3. The expenses of such removal and abatement shall be paid and may be collected in the manner prescribed in sections 1306, 1307, and 1308 of the New York State Public Health Law.
