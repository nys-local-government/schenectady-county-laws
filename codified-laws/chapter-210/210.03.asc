[[codified-section-210-03, 210.3]]
=== Section 210.03. Parking, standing and stopping.

A. No person shall park, stand or stop a vehicle at any place within county property except those places where parking, standing or stopping shall be permitted by order of the director of facilities, or his or her designee.

B. The director of facilities may by order designate areas where parking, standing or stopping shall be restricted to vehicles operated by county officials or personnel or others having official business or performing special services at any of the county or library buildings or court house.
