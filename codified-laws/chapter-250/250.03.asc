[[codified-section-250-03, 250.3]]
=== Section 250.03. Fee.footnote:[As amended 12-21-2009 by L.L. 4-2009; 12-22-04 by L.L. 9-2004]

A. For recording, entering, indexing and endorsing a certificate on any instrument, twenty dollars

B. Five dollars for each page or portion of a page.

C. For the purpose of determining the appropriate recording fee, the fee for any cover page shall be deemed an additional page of the instrument.
