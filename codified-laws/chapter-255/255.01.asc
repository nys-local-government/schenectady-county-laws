[[codified-section-255-01, 255.1]]
=== Section 255.01. Legislative intent.

A. Chapter 481 of the Laws of 1991 of the State of New York amended the New York State Correction Law adding section 500-h to authorize counties to enact a local law which would allow such counties to be reimbursed for costs paid for medical, hospital or dental services provided to inmates of such counties' correctional facility from any third party coverage or indemnification carried by such inmates.

B. This legislature does hereby find that it is appropriate for the County of Schenectady to seek whatever reimbursement is legally available for medical, hospital and dental care delivered to inmates of the county jail.

C. It is the intention of this legislature in enacting this local law to enact the provisions of section 500-h(2) of the New York State Correction Law and thereby enable the county to be reimbursed for medical, hospital and dental care for those inmates of the Schenectady County jail having third party coverage or indemnification.
