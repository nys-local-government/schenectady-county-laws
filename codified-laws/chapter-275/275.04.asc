[[codified-section-275-04, 275.4]]
=== SECTION 275.04. RESPONSIBILITY FOR ADMINISTRATION.

The Stormwater Management Officer(s) (SMO(s)) shall administer, implement, and enforce the provisions of this law.
Such powers granted or duties imposed upon the authorized enforcement official may be delegated in writing by the SMO as may be authorized by the County Legislature or County Manager.
