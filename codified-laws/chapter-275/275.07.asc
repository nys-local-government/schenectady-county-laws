[[codified-section-275-07, 275.7]]
=== SECTION 275.07. PROHIBITION AGAINST FAILING INDIVIDUAL SEWAGE TREATMENT SYSTEMS

No persons shall operate a failing individual sewage treatment system in areas tributary to the municipality’s MS4. A failing individual sewage treatment system is one which has one or more of the following conditions:

A. The backup of sewage into a structure.

B. Discharges of treated or untreated sewage onto the ground surface.

C. A connection or connections to a separate stormwater sewer system.

D. Structural failure of any component of the individual sewage treatment system that could lead to any of the other failure conditions as noted in this section.

E. Contamination of off-site groundwater.
