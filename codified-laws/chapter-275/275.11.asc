[[codified-section-275-11, 275.11]]
=== SECTION 275.11. INDUSTRIAL OR CONSTRUCTION ACTIVITY DISCHARGES.

Any person subject to an industrial or construction activity SPDES stormwater discharge permit shall comply with all provisions of such permit.
Proof of compliance with said permit may be required in a form acceptable to the municipality prior to the allowing of discharges to the MS4.
