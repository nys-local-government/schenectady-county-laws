[[codified-section-275-17, 275.17]]
=== SECTION 275.17. INJUNCTIVE RELIEF.

It shall be unlawful for any person to violate any provision or fail to comply with any of the requirements of this law.
If a person has violated or continues to violate the provisions of this law, the SMO may petition for a preliminary or permanent injunction restraining the person from activities which would create further violations or compelling the person to perform abatement or remediation of the violation.
