[[codified-section-285-03, 285.3]]
=== Section 285.03. Scope and applicability.

This Chapter shall apply to menu items that are served in portions the size and content of which are standardized at a chain food service establishment.
This Chapter shall not apply to menu items that are listed on a menu or menu board for less than 30 days in a calendar year.

A. Posting calorie information for menu items.
+
All menu boards and menus in any chain food service establishment shall bear the total number of calories derived from any source for each menu item they list.
Such information shall be listed as “calories” or “cal” clearly and conspicuously, adjacent or in close proximity such as to be clearly associated with the menu item, using a font and format that is at least as prominent in size and appearance, as that used to post either the name or price of the menu item.

B. Calculating calories.
+
Calorie content values (in kcal) required by this Chapter shall be based upon a verifiable analysis of the menu item, which may include the use of nutrient databases, laboratory testing, or other reliable methods of analysis, and shall be rounded to the nearest ten (10) calories for calorie content values above fifty (50) calories, and to the nearest five (5) calories for calorie content values fifty (50) calories and below.

C. Food item tags.
+
When a food item is displayed for sale with a food item tag, such food item tag shall include the calorie content value for that food item in a font size and format at least as prominent as the font size of the name of the food item.

D. Drive-through windows.
+
Calorie content values at drive-through windows shall be displayed on either the drive through menu board, or on an adjacent stanchion visible at or prior to the point of ordering, so long as the calorie content values are as clearly and conspicuously posted on the stanchion adjacent to their respective menu item names, as the price or menu item is on the drive through menu board.

E. This chapter does not preclude any establishment, including chain food service establishments, from voluntarily providing additional nutritional information, nor from providing a disclaimer stating that there may be variations in calorie content values across service based on slight variations in serving size, quantity of ingredients, or special ordering.
