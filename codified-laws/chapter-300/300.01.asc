[[codified-section-300-01, 300.1]]
=== Section 300.01. Legislative intent.

Pursuant to the authority of section 253-r of the New York State Tax Law, there is hereby imposed and there shall be paid a tax of twenty-five cents (25¢) for each one hundred dollars ($100) and each remaining major fraction thereof of principal debt or obligation which is or under any contingency may be secured at the date of execution thereof, or at any time thereafter, by a mortgage on real property situated within Schenectady County and recorded on or after the date upon which such tax takes effect and a tax of twenty-five cents (25¢) on such mortgage if the principal debt or obligation which is or by any contingency may be secured by such mortgage is less than one hundred dollars ($100).
