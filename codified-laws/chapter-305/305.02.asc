[[codified-section-305-02, 305.2]]
=== Section 305.02. Agricultural assessment.

The County of Schenectady by this local law hereby affirmatively approves the granting of the agricultural assessment to commercial horse boarding operations, as defined by all relevant New York State statutes and opinions, within Schenectady County.
This exemption shall not be construed to include operations whose primary on-site function is horse racing.
