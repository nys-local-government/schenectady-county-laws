[[codified-section-315-03, 315.3]]
=== Section 315.03.

An exemption shall be granted only upon written application by the owner of such building on a form approved by the appropriate assessor.
The application shall be filed with the assessor of the city, town, or village having the power to assess property for taxation on or before the appropriate taxable status date of such city, town, or village.
