[[codified-section-315-04, 315.4]]
=== Section 315.04.

If satisfied that the applicant is entitled to an exemption pursuant to this local law, the assessor shall approve the application and such building shall hereafter be exempt from taxation and special ad valorem levies as herein provided commencing with the assessment roll prepared on the basis of the taxable status date referred to in section three of this local law.
The assessed value of any exemption granted pursuant to this section shall be entered by the assessor on the assessment roll with the taxable property, with the amount of the exemption shown in a separate column.
