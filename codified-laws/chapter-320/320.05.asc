[[codified-section-320-05, 320.5]]
=== Section 320.05. Application beginning in 2006.footnote:[Amended 2-14-2006 by L.L. 1-2006]

This local law shall apply to assessment rolls based on taxable status dates occurring on or after January 2, 2006.
