== Chapter 340.footnote:[Adopted 12-14-1999 by Local Law 8-1999.] TAX: REAL PROPERTY—TAX EXEMPTION FOR HISTORIC PROPERTIES

include::chapter-340/340.01.asc[]

include::chapter-340/340.02.asc[]

include::chapter-340/340.03.asc[]

include::chapter-340/340.04.asc[]
