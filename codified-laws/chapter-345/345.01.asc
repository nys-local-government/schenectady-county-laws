[[codified-section-345-01, 345.1]]
=== Section 345.01. Exemption.

A. Pursuant to section 466-e of the New York State Real Property Tax Law, real property owned by a person, or owned by such person and his or her spouse, who is an enrolled member of an incorporated volunteer fire company, fire department or incorporated volunteer ambulance service or who is an enrolled member of an incorporated volunteer fire company or fire department serving in the auxiliary of such company or department shall be exempt from taxation by the County of Schenectady to the extent of 10% of the assessed value of such property for county purposes, exclusive of special assessments, provided, however, that such exemption shall in no event exceed three thousand dollars ($3,000) multiplied by the latest state equalization rate for the assessing unit in which such real property is located.

B. Such exemption shall not be granted unless:

1. The person applying for the exemption resides in the city, town or village which is served by such incorporated volunteer fire company or fire department or incorporated voluntary ambulance services;

2. The property is the primary residence of the person applying for the exemption;

3. The property is used exclusively for residential purposes, provided, however, that in the event any portion of such property is not used exclusively for the applicant's residence but is used for other purposes, such portion shall be subject to taxation and the remaining portion only shall be entitled to the exemption provided by this local law; and

4. The person applying for the exemption has been certified by the authority having jurisdiction for the incorporated volunteer fire company or fire department as an enrolled member of such incorporated volunteer fire company or fire department for at least 5 years or has been certified by the authority having jurisdiction for the incorporated volunteer fire company or fire department as an enrolled member serving in the auxiliary of such incorporated volunteer fire company or fire department for at least 5 years, or has been certified by the authority having jurisdiction for the incorporated voluntary ambulance service as an enrolled member of such incorporated voluntary ambulance service for at least 5 years;
[lowerroman]
.. For county purposes, the procedure for certification by the appropriate authority shall be determined by the assessors designated by the respective municipalities, school districts or fire districts.

C. On or after the effective date of this local law, any enrolled member of an incorporated volunteer fire company, fire department or incorporated voluntary ambulance service or any enrolled member of an incorporated volunteer fire company or fire department serving in the auxiliary of such company or department who accrues more than 20 years of active service and is so certified by the authority having jurisdiction for the incorporated volunteer fire company, fire department or incorporated voluntary ambulance service, shall be granted the 10% exemption as authorized by this local law for the remainder of his or her life as long as his or her primary residence is located within Schenectady County.

D. Application for such exemption shall be filed with the assessor designated by the municipality, school district and/or fire district on or before the exemption filing deadline date on a form as prescribed by the state board.

E. No applicant who is a volunteer firefighter or volunteer ambulance worker who by reason of such status is receiving any benefit under the provisions of article 4 of the New York State Real Property Tax Law on January 1, 2005 shall suffer any diminution of such benefit because of the provisions of this local law.
