== Chapter 365.footnote:[Adopted 1-9-1996 by Local Law 1-1996. Amendments noted where applicable.] TAX: REAL PROPERTY—TAX LIEN REDEMPTION PERIOD

include::chapter-365/365.01.asc[]

include::chapter-365/365.02.asc[]

include::chapter-365/365.03.asc[]

include::chapter-365/365.04.asc[]
