[[codified-section-365-01, 365.1]]
=== Section 365.01. Effective date.

The provisions of this local law shall only be applicable to the enforcement of collection of delinquent taxes by Schenectady County of all taxes which shall have become liens on or after January 1, 1995.
