[[codified-section-375-07, 375.7]]
=== Section 375.07. Reward.

The enforcement officer shall offer and the county legislature may authorize by resolution the payment of a reward of one hundred dollars ($100) to any person, not employed by local or county government and not a member of a state or local police department, for information which shall lead to the arrest and conviction of a person or persons in a criminal proceeding of selling or causing to be sold tobacco products to a child less than 18 years of age in the County of Schenectady in violation of section 260.21(3) of the New York State Penal Law.
