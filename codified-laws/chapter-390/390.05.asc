[[codified-section-390-05, 390.5]]
=== Section 390.05. Annual accounting required.

Each wireless communications service supplier shall annually provide to the County of Schenectady an accounting of the surcharge amounts billed and collected.
