<?xml version='1.0' encoding="iso-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

  <xsl:template match="xref" name="xref">
    <xsl:variable name="target" select="key('id',@linkend)[1]"/>

    <xsl:call-template name="check.id.unique">
      <xsl:with-param name="linkend" select="@linkend"/>
    </xsl:call-template>

    <xsl:variable name="xrefstyle">
      <xsl:choose>
        <xsl:when test="@role and not(@xrefstyle)
                        and $use.role.as.xrefstyle != 0">
          <xsl:value-of select="@role"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="@xrefstyle"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
    <xsl:when test="count($target)=0">
      <xsl:message>
        <xsl:text>XRef to nonexistent id: </xsl:text>
        <xsl:value-of select="@linkend"/>
      </xsl:message>
      <xsl:text>[?]</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:variable name="text">
        <xsl:apply-templates select="." mode="xref.text"/>
      </xsl:variable>

      <!-- how to print it -->
      <xsl:choose>
      <xsl:when test="$text!=''">
        <xsl:call-template name="hyperlink.markup">
          <xsl:with-param name="linkend" select="@linkend"/>
          <xsl:with-param name="text" select="$text"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="$target" mode="xref-to">
          <xsl:with-param name="referrer" select="."/>
          <xsl:with-param name="xrefstyle" select="$xrefstyle"/>
        </xsl:apply-templates>
      </xsl:otherwise>
      </xsl:choose>
    </xsl:otherwise>
    </xsl:choose>

    <!-- Add standard page reference? -->
    <xsl:choose>
    <xsl:when test="self::biblioref">
      <!-- no page number for a biblio reference -->
    </xsl:when>
    <xsl:when test="starts-with(normalize-space($xrefstyle), 'select:')
                    and contains($xrefstyle, 'nopage')">
      <!-- negative xrefstyle in instance turns it off -->
    </xsl:when>
    <xsl:when test="not(starts-with(normalize-space($xrefstyle), 'select:')
                  and (contains($xrefstyle, 'page')
                       or contains($xrefstyle, 'Page')))
                  and ( $insert.xref.page.number = 'yes'
                     or $insert.xref.page.number = '1')
                  or local-name($target) = 'para'">
      <xsl:apply-templates select="$target" mode="page.citation">
        <xsl:with-param name="id" select="@linkend"/>
      </xsl:apply-templates>
    </xsl:when>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
